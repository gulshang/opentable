from __future__ import unicode_literals

from django.db import models
from jsonfield import JSONField


class Artist(models.Model):
    """
    Description: will represent Actors, Directors, Writers etc.
    Fields:
        name: Name of the Artist
    """
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Location(models.Model):
    """
    Description: Will represent the location where movie were screened
    Fields:
        name: Name of the location
    """
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class ProductionCompany(models.Model):
    """
    Description: Will be used for the representing the Production Company
    Fields:
        name: Name of the Production Company
    """
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Distributor(models.Model):
    """
    Description: Will be representing the Distributor of the movie
    Fields:
        name: Name of the distributor of the company
    """
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Movie(models.Model):
    """
    Description: Will be representing the movie details including the filming
        location, artists involved etc
    Fields:
        title: Title of the movie
        release_year: Release year of the movie
        actors: All the actors whose data is available
        director: Director of the movie
        production_company: Production company of the movie
        distributor: Distributor of the company
        extra: Extra parameters will be stored here like fun facts etc.
        locations: Locations where the film was filmed
    """
    title = models.CharField(max_length=50)
    release_year = models.CharField(max_length=4)
    actors = models.ManyToManyField('Artist', related_name='enacted_in_movies')
    director = models.ForeignKey('Artist', related_name='directed_movies')
    writers = models.ManyToManyField('Artist', related_name='movies_written')
    production_company = models.ForeignKey('ProductionCompany',
                                           related_name='movies_produced')
    distributors = models.ForeignKey('Distributor',
                                     related_name='movies_distributed')
    locations = models.ManyToManyField('Location',
                                       related_name='movies_filmed')
    extra = JSONField(default={})

    def __unicode__(self):
        return "%s (%s)" % (self.title, self.release_year)
