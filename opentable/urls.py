from django.conf.urls import include, url, patterns
from django.contrib import admin
from foodtruck import views as foodtruck_views

urlpatterns = patterns(
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/$', foodtruck_views.logout, name='logout'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^$', foodtruck_views.index, name='index'),
    (r'^foodtruck/', include('foodtruck.urls')),
)
