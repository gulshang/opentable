from django.conf.urls import url, patterns
import views as foodtruck_views

urlpatterns = patterns(
    'foodtruck.views',
    url(r'^get-nearby-foodtrucks/$', foodtruck_views.get_nearby_foodtrucks,
        name='get_nearby_foodtrucks'),
)
