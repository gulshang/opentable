# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import djorm_pgfulltext.fields
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Applicant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='AvailableHour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_hour', models.IntegerField()),
                ('day', models.CharField(max_length=12, choices=[('SUNDAY', 'Sunday'), ('MONDAY', 'Monday'), ('TUESDAY', 'Tuesday'), ('WEDNESDAY', 'Wednesday'), ('THURSDAY', 'Thursday'), ('FRIDAY', 'Friday'), ('SATURDAY', 'Saturday')])),
            ],
        ),
        migrations.CreateModel(
            name='FoodItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60)),
                ('search_index', djorm_pgfulltext.fields.VectorField(default=b'', serialize=False, null=True, editable=False, db_index=True)),
            ],
        ),
        migrations.CreateModel(
            name='FoodTruck',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('facility_type', models.CharField(max_length=20, choices=[('TRUCK', 'Truck'), ('PUSH_CART', 'Push Cart'), ('UNKNOWN', 'Unknown')])),
                ('is_prior_permit', models.BooleanField(default=False)),
                ('extra', jsonfield.fields.JSONField(default=dict)),
                ('applicant', models.ForeignKey(to='foodtruck.Applicant')),
                ('available_slots', models.ManyToManyField(to='foodtruck.AvailableHour', blank=True)),
                ('fooditems', models.ManyToManyField(to='foodtruck.FoodItem')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location_id', models.CharField(max_length=20)),
                ('description', models.TextField()),
                ('address', models.TextField()),
                ('block', models.CharField(max_length=10)),
                ('lot', models.CharField(max_length=10)),
                ('x_axis', models.DecimalField(null=True, max_digits=28, decimal_places=14, blank=True)),
                ('y_axis', models.DecimalField(null=True, max_digits=28, decimal_places=14, blank=True)),
                ('latitude', models.DecimalField(null=True, max_digits=28, decimal_places=14, blank=True)),
                ('longitude', models.DecimalField(null=True, max_digits=28, decimal_places=14, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Permit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('permit_id', models.CharField(max_length=15)),
                ('status', models.CharField(max_length=15, choices=[('APPROVED', 'Approved'), ('EXPIRED', 'Expired'), ('INACTIVE', 'Inactive'), ('REQUESTED', 'Requested'), ('SUSPEND', 'Suspend')])),
                ('noi_sent_date', models.DateField(null=True, blank=True)),
                ('approved_on', models.DateTimeField(null=True, blank=True)),
                ('receieved_on', models.DateTimeField(null=True, blank=True)),
                ('expiration_date', models.DateField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='foodtruck',
            name='location',
            field=models.ForeignKey(to='foodtruck.Location'),
        ),
        migrations.AddField(
            model_name='foodtruck',
            name='permit',
            field=models.ForeignKey(to='foodtruck.Permit'),
        ),
    ]
