import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

import datetime
from foodtruck import models as foodtruck_models

f = open('/tmp/Mobile_Food_Facility_Permit.csv', 'r')

headings = [
    'locationid',
    'applicant',
    'facilityType',
    'cnn',
    'locationDescription',
    'address',
    'blocklot',
    'block',
    'lot',
    'permit',
    'status',
    'foodItems',
    'x',
    'y',
    'latitude',
    'longitude',
    'schedule',
    'dayshours',
    'nOISent',
    'approved',
    'received',
    'priorPermit',
    'expirationDate',
    'location',
]

for line in f:
    line = line.split('$')
    d = {}
    if line[0] == 'locationid':
        continue
    for index, item in enumerate(line):
        d.update({headings[index]: item})
    print d

    loc = foodtruck_models.Location()
    loc.location_id = d['locationid']
    loc.description = d['locationDescription']
    loc.address = d['address']
    loc.block = d['block']
    loc.lot = d['lot']
    loc.x_axis = float(d['x']) if d['x'] else None
    loc.y_axis = float(d['y']) if d['y'] else None
    loc.latitude = float(d['latitude']) if d['latitude'] else None
    loc.longitude = float(d['longitude']) if d['longitude'] else None
    loc.save()

    fooditems_list = d['foodItems'].split(':')
    fooditems_objs_list = []
    for item in fooditems_list:
        fi, _ = foodtruck_models.FoodItem.objects.get_or_create(
            name=item[:60].title()
        )
        fooditems_objs_list.append(fi)

    applicant, _ = foodtruck_models.Applicant.objects.get_or_create(
        name=d['applicant']
    )

    permit, _ = foodtruck_models.Permit.objects.get_or_create(
        permit_id=d['permit']
    )
    permit.status = d['status']
    if d['nOISent'].strip():
        permit.noi_sent_date = datetime.datetime.strptime(
            d['nOISent'].strip()[:10],
            "%m/%d/%Y"
        )
    if d['received'].strip():
        permit.received_on = datetime.datetime.strptime(
            d['received'].strip(),
            "%b %d %Y %I:%M%p"
        )
    if d['approved'].strip():
        permit.approved_on = datetime.datetime.strptime(
            d['approved'].strip()[:10],
            "%m/%d/%Y"
        )
    if d['expirationDate'].strip():
        permit.expiration_date = datetime.datetime.strptime(
            d['expirationDate'].strip()[:10],
            "%m/%d/%Y"
        )
    permit.save()

    ft = foodtruck_models.FoodTruck()
    ft.facility_type = d['facilityType'].strip().replace(' ', '_').upper()
    ft.location = loc
    ft.applicant = applicant
    ft.permit = permit
    ft.is_prior_permit = bool(d['priorPermit'].strip())
    ft.extra = {
        'cnn': d['cnn'],
        'schedule': d['schedule'],
        'dayshours': d['dayshours'],
    }
    ft.save()
    ft.fooditems.add(*fooditems_objs_list)


f.close()
