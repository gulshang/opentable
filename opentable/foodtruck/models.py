from __future__ import unicode_literals

from django.db import models
from jsonfield import JSONField
from djorm_pgfulltext.models import SearchManager
from djorm_pgfulltext.fields import VectorField


class Location(models.Model):
    location_id = models.CharField(max_length=20)
    description = models.TextField()
    address = models.TextField()
    block = models.CharField(max_length=10)
    lot = models.CharField(max_length=10)
    x_axis = models.DecimalField(max_digits=28, decimal_places=14,
                                 null=True, blank=True)
    y_axis = models.DecimalField(max_digits=28, decimal_places=14,
                                 null=True, blank=True)
    latitude = models.DecimalField(max_digits=28, decimal_places=14,
                                   null=True, blank=True)
    longitude = models.DecimalField(max_digits=28, decimal_places=14,
                                    null=True, blank=True)

    def __unicode__(self):
        return "%s (%s, %s)" % (self.location_id, self.latitude, self.longitude)

    def mini_json(self):
        return {
            'id': self.id,
            'address': self.address,
            'lat': self.latitude,
            'lng': self.longitude,
        }


class FoodItem(models.Model):
    name = models.CharField(max_length=60)
    search_index = VectorField()
    objects = SearchManager(
        fields=('name'),
        config='pg_catalog.english',
        search_field='search_index',
        auto_update_search_field=True
    )

    def __unicode__(self):
        return self.name


class AvailableHour(models.Model):
    SUNDAY = 'SUNDAY'
    MONDAY = 'MONDAY'
    TUESDAY = 'TUESDAY'
    WEDNESDAY = 'WEDNESDAY'
    THURSDAY = 'THURSDAY'
    FRIDAY = 'FRIDAY'
    SATURDAY = 'SATURDAY'

    DAY_CHOICES = (
        (SUNDAY, 'Sunday'),
        (MONDAY, 'Monday'),
        (TUESDAY, 'Tuesday'),
        (WEDNESDAY, 'Wednesday'),
        (THURSDAY, 'Thursday'),
        (FRIDAY, 'Friday'),
        (SATURDAY, 'Saturday'),
    )

    start_hour = models.IntegerField()
    day = models.CharField(max_length=12, choices=DAY_CHOICES)

    def __unicode__(self):
        return "%s - %s" % (self.hourslot, self.day)


class Applicant(models.Model):
    name = models.CharField(max_length=80)

    def __unicode__(self):
        return self.name


class Permit(models.Model):
    APPROVED = 'APPROVED'
    EXPIRED = 'EXPIRED'
    INACTIVE = 'INACTIVE'
    REQUESTED = 'REQUESTED'
    SUSPEND = 'SUSPEND'

    STATUS_CHOICES = (
        (APPROVED, 'Approved'),
        (EXPIRED, 'Expired'),
        (INACTIVE, 'Inactive'),
        (REQUESTED, 'Requested'),
        (SUSPEND, 'Suspend'),
    )
    permit_id = models.CharField(max_length=15)
    status = models.CharField(max_length=15, choices=STATUS_CHOICES)
    noi_sent_date = models.DateField(null=True, blank=True)
    approved_on = models.DateTimeField(null=True, blank=True)
    receieved_on = models.DateTimeField(null=True, blank=True)
    expiration_date = models.DateField(null=True, blank=True)


class FoodTruck(models.Model):
    TRUCK = 'TRUCK'
    PUSH_CART = 'PUSH_CART'
    UNKNOWN = 'UNKNOWN'
    FACILITY_TYPE_CHOICES = (
        (TRUCK, 'Truck'),
        (PUSH_CART, 'Push Cart'),
        (UNKNOWN, 'Unknown'),
    )
    FACILITY_TYPE_VERBOSE_MAP = {i[0]: i[1] for i in FACILITY_TYPE_CHOICES}
    facility_type = models.CharField(max_length=20,
                                     choices=FACILITY_TYPE_CHOICES)
    fooditems = models.ManyToManyField('FoodItem')
    location = models.ForeignKey('Location')
    available_slots = models.ManyToManyField('AvailableHour', blank=True)
    applicant = models.ForeignKey('Applicant')
    permit = models.ForeignKey('Permit')
    is_prior_permit = models.BooleanField(default=False)
    extra = JSONField()

    def __unicode__(self):
        return self.applicant.name

    def mini_json(self):
        return {
            'facility_type': self.FACILITY_TYPE_VERBOSE_MAP[self.facility_type],
            'food_items': [{
                'id': fd.id, 'name': fd.name
                } for fd in self.fooditems.all()],
            'location': self.location.mini_json(),
            'applicant': self.applicant.name,
        }
