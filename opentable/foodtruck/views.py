from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import JsonResponse
import models as foodtruck_models
from math import radians, cos, sin, asin, sqrt
from django.db.models import Q
from django.contrib.auth.decorators import login_required

LNG_RANGE = 0.003
LAT_RANGE = 0.0015


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km


def index(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return redirect(reverse('admin/'))
        return render(request, "foodtruck/index.html", {
        })
    else:
        return redirect(reverse("login"))


@login_required
def logout(request):
    return redirect(reverse("login"))


@login_required
def get_nearby_foodtrucks(request):
    fooditems_query = request.GET.get('query')
    mylat = float(request.GET['lat'])
    mylng = float(request.GET['lng'])
    q = Q(
        location__latitude__lte=mylat+LAT_RANGE,
        location__latitude__gte=mylat-LAT_RANGE,
        location__longitude__lte=mylng+LAT_RANGE,
        location__longitude__gte=mylng-LNG_RANGE
    )
    ftrucks = foodtruck_models.FoodTruck.objects.filter(q)
    if fooditems_query:
        ftrucks = ftrucks.filter(
            fooditems__in=foodtruck_models.FoodItem.objects.search(
                fooditems_query))

    return JsonResponse({
        'foodtrucks': [fd.mini_json() for fd in ftrucks],
        'success': True,
    })
