var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
var labelIndex = 0;
var markersArr = [];
var fds;
var mylocation;
var map;
var pos;
var infoWindow;

function get_nearby_foodtrucks_location(map, position, query){
    $.ajax({
        type: 'GET',
        url: "/foodtruck/get-nearby-foodtrucks/",
        data: {
            'lat': position.lat,
            'lng': position.lng,
            'query': query,
        },
        success: function(data){
            fds = data.foodtrucks;
            var label;
            var d;
            clearAllMarkers(map);
            $('#foodtrucks_list').html('');
            if(data.foodtrucks.length == 0){
                $('#foodtrucks_list').html('<h2>No Results Nearby your area</h2>');
            }
            for(var i=0; i<data.foodtrucks.length; i++){
                fd = data.foodtrucks[i];
                $('#foodtrucks_list').html($('#foodtrucks_list').html() + create_foodtruck_element(fd))
                loc = {
                    'lat': parseFloat(fd.location.lat),
                    'lng': parseFloat(fd.location.lng),
                };
                addMarker(map, loc, fd);
            }
        }
    });
}

$('#search_foodtrucks_btn').on('click', function(){
    var query = $('#search_query').val();
    if(query){
        get_nearby_foodtrucks_location(map, pos, query)
        $('#show_result_caption').show();
        $('#copy_query').text(query);
    }
});

function clearAllMarkers(map){
    for(var i=0; i<markersArr.length; i++){
        markersArr[i].setMap(null);
    }
}

function addMarker(map, loc, fd) {
    var marker = new google.maps.Marker({
        position: loc,
        label: labels[labelIndex++ % labels.length],
        map: map
    });
    markersArr.push(marker);
    marker.addListener('click', function() {
        var infoWindow = new google.maps.InfoWindow({map: map});
        infoWindow.setPosition(marker.getPosition());
        infoWindow.setContent("<h3>" + fd.applicant + "</h3><p>"+ fd.location.address +"</p>");
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 37.709375464, lng: -122.3733025775},
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    infoWindow = new google.maps.InfoWindow({map: map});

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            mylocation = pos;

            map.setCenter(pos);
            get_nearby_foodtrucks_location(map, pos, null);
            infoWindow.setPosition(pos);
            infoWindow.setContent("<h3>You are here!</h3>");

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    var markersArr = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        $('#foodtrucks_list').html('');
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            pos = place.geometry.location.toJSON();
            get_nearby_foodtrucks_location(map, pos, null);
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };
            infoWindow.setPosition(pos);
            infoWindow.setContent(
                "<h3>" + place.name + "</h3>" +
                place.adr_address + "<br>" +
                "<a href=" + place.url + " target='_blank'>View on Google maps</a>"
            );

            // Create a marker for each place.
            markersArr.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
        map.setZoom(17);
    });
}

function processLocation(map, location, infoWindow){

}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
}

function create_foodItem_element(foodItems){
    var ele_list = '';
    for(var i=0; i<foodItems.length; i++){
        ele_list += "<li>" + foodItems[i].name + "</li>";
    }
    return ele_list;
}

function create_foodtruck_element(foodTruck){
    var foodItemsEleList = "<li class='list-group-item'>" +
            "<h3>" + foodTruck.applicant + "</h3>" +
            "<div id='food_items'><u>Other FoodItems Available</u>" +
                "<ul>" +
                create_foodItem_element(foodTruck.food_items) +
                "</ul>" +
            "</div>" +
    "</li>"
    return foodItemsEleList;
}

$('#reset_query').click(function(){
    get_nearby_foodtrucks_location(map, mylocation, null);
    $('#show_result_caption').hide();
});
