from fabric.api import run, local, cd, task, env, prefix
from fabric.operations import prompt
from contextlib import contextmanager as _contextmanager
import os
import subprocess

DEPLOYMENT_CONFIG = {
    'production': {
        'DIR': "/home/projects/opentable",
        'env': {
            'user': 'ubuntu',
            'host_string': 'gulshang.com',
            'forward_agent': True,
        },
    },
}


def execute_unix(inputcommand):
    p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    return output


@_contextmanager
def virtualenv(venv):
    cmd = "source %s" % os.path.join(venv, "bin/activate")
    with prefix(cmd):
        yield


def _set_environment(env_config):
    env.user = env_config.get('user')
    env.host_string = env_config.get('host_string')
    env.forward_agent = env_config.get('forward_agent')


def setup(deploy_dir):
    venv = os.path.join(deploy_dir, "envproj")
    with cd(os.path.join(deploy_dir)):
        with virtualenv(venv):
            run("pip install -r requirements.txt")

    with cd(os.path.join(deploy_dir, "opentable")):
        with virtualenv(venv):
            run("python manage.py migrate")

    # restart services
    with cd(deploy_dir):
        with virtualenv(venv):
            print "\t >>>>> RESTARTING GUNICORN <<<<<<<<"
            run("supervisorctl -c confs/supervisord.conf restart gunicorn")


def _deploy(server_name):

    config = DEPLOYMENT_CONFIG[server_name]
    speech = "echo 'Deploying Master to %s' | espeak -s 150" % server_name
    execute_unix(speech)
    response = prompt("THIS WILL DEPLOY MASTER TO %s. ARE YOU SURE??? Type 'yes' to confirm ===> " % server_name.upper())
    if response.lower() == "yes":
        try:
            deploy_dir = config.get('DIR')
            _set_environment(config.get('env'))
            try:
                from pyfiglet import Figlet
            except ImportError:
                print "Please install pyfiglet: pip install pyfiglet"
            else:
                local("clear")
                f = Figlet(font='slant')
                print f.renderText("HOLD YOUR BREATH")
                with cd(deploy_dir):
                    run("git pull origin master")
                setup(deploy_dir)
                print f.renderText("DID SHIT HIT THE ROOF?")
        except Exception, e:
            print e


def _rollback(config):
    deploy_dir = config.get('DIR')
    print ">>>> ROLLING BACK <<<<"
    with cd(deploy_dir):
        run("git reset --hard ORIG_HEAD")

    setup(deploy_dir)


@task
def deploy_production():
    _deploy('production')


@task
def rollback_servers():
    for server_name, deploy_config in DEPLOYMENT_CONFIG.items():
        response = prompt("THIS WILL ROLLBACK MASTER IN %s. ARE YOU SURE??? Type 'yes' to confirm ===> " % server_name.upper())
        if response.lower() == "yes":
            _rollback(deploy_config)
